package net.tngou.db.netty;

import java.util.concurrent.TimeUnit;

import net.tngou.db.lucene.Config;
import net.tngou.db.lucene.LuceneManage;
import net.tngou.db.util.I18N;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 *  重启服务器
* @ClassName: StopServer
* @Description: TODO(这里用一句话描述这个类的作用)
* @author tngou@tngou.net
* @date 2015年5月14日 上午9:48:06
*
 */
public class RestartServer extends Netty implements Runnable{
	private static Logger log = LoggerFactory.getLogger(RestartServer.class);
	@Override
	public void run() {
		
		try {
		
			 Parameters params = new Parameters();
				FileBasedConfigurationBuilder<?> builder =
				    new FileBasedConfigurationBuilder<>(PropertiesConfiguration.class)
				    .configure(params.properties()
				        .setFileName("config.properties"));
			Configuration config = builder.getConfiguration();

			config.setProperty("run", 0);
			builder.save();
		
		} catch (ConfigurationException  e) {
			log.error(I18N.getValue("stop_error","config.properties"));
			e.printStackTrace();
		}finally
		{
			Thread.yield();
		}
		
		
	}
	
	public static void main(String[] args) throws InterruptedException {
		new StopServer().run();
		TimeUnit.MILLISECONDS.sleep(12000);//12s 重启时间
		int port =  Config.getInstance().getPort();
        Thread t = new Thread(new StartServer(port));
        t.start();      
        log.info(I18N.getValue("restart",port+"")); 
        Thread td = new Thread(new Daemons());
        td.setDaemon(true);  //设置后台监听程序
        td.start();
	}

}
